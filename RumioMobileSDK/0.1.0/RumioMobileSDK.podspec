#
# Be sure to run `pod lib lint RumioMobileSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RumioMobileSDK'
  s.version          = '0.1.0'
  s.summary          = 'This is a scan library'
  s.swift_version    = '5.0'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  s.homepage         = 'https://harolgomz1@bitbucket.org/harolgomz1/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'harol.gomez' => 'harol.gomez@rappi.com' }
  s.source           = { :git => 'git@bitbucket.org:rappinc/rumio-sdk-ios.git', :branch => "develop"}
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'

  s.source_files = 'RumioMobileSDK/Classes/**/*'
  s.resources  = ['RumioMobileSDK/Assets/Assets.xcassets','RumioMobileSDK/Classes/Resources/**/*']
  s.dependency 'lottie-ios', '~> 3.1'
  s.dependency 'Alamofire'
  s.dependency 'SDWebImage'
  s.dependency 'FLAnimatedImage', '~> 1.0'
#   s.resource_bundles = {
#     'RumioMobileSDK' => ['RumioMobileSDK/Assets/Assets.xcassets']
#   }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
